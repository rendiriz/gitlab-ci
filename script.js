#!/usr/bin/env node

const app = process.argv[2];

function checkAffected() {
  const array = ["admin"];
  return array.includes(app) ? app : "break";
}

process.stdout.write(checkAffected());
